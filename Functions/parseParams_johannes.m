function [rAll, rUpd, rNew, cDef] = parseParams_johannes(xDef, varargin)

% prepare default struct
if isstruct(xDef)
  rDef = xDef;
elseif isobject(xDef)  
  cProp = fieldnames(xDef);
  for iProp = 1 : length(cProp)
    tProp        = cProp{iProp};
    rDef.(tProp) = xDef.(tProp);
  end  
else
  error( 'parseParams:invalid_argument',...
         'First input argument must be a struct or an object.' );
end

% create parameter parser object
hParser = inputParser;

% function handle to add parameters
if ismethod(hParser, 'addParameter')
  hAdd = @hParser.addParameter;
else
  hAdd = @hParser.addParamValue; %#ok (used before 2013b)
end

% add parameters
cFld = fieldnames(rDef);
nFld = length(cFld);
for iFld = 1 : nFld
  hAdd(cFld{iFld}, rDef.(cFld{iFld}));
end

% configure parser
hParser.KeepUnmatched = true;

% parse
hParser.parse(varargin{:});

% get output
if isa(xDef, 'struct')

    rUpd = hParser.Results;
    rNew = hParser.Unmatched;
    cDef = hParser.UsingDefaults;
    
    % keep order of fields as in default struct
    rUpd = orderfields(rUpd, rDef);
    
    % concatenate rUpd and rNew
    rAll = rUpd;
    cFld = fieldnames(rNew);
    for iFld = 1:length(cFld)
        tFld = cFld{iFld};
        rAll.(tFld) = rNew.(tFld);
    end

else
      % loop over the fieldnames of the struct
     % and set the properties of the object appropriately
     fn = fieldnames(hParser.Results);
     pn = fieldnames(xDef);
     for kk = 1:length(pn)
        pnkk = pn{kk}; 
        eval(['xDef.' pnkk '= hParser.Results.' pnkk ';']); 
        rUpd = xDef;
        rNew = [];
        cDef = [];
        rAll = [];
     end
end



