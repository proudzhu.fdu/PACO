classdef audiosignal
    %AUDIOSIGNAL Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        framelength = 32e-3;
        frameshift = 4e-3;
        fs = 16e3;
        N 
        L 
        nfft
        data
        nsamples
        nchannels
        siglength
        sampletimes
        sampleindex
        window = @(N) sqrt(hamming(N,'periodic'));
        framed_t
        framed_f
        framed_f_full
        nframes
        f_axis
        f_axes_PA
        t_axis
        win
        f0 = [];
        t_f0 = [];
        framed_f_PS = [];
        framed_f_PS_pn = [];
        t_PS = [];
        framed_f_PA = [];
        framed_f_PA_pn = [];
        FFT_factor = 1;
    end
    
    methods
        function obj = audiosignal(data, varargin) % Class Construction method
            obj.data = data;
            [~, obj, ~, ~] = parseParams_johannes(obj,varargin{:}); % set properties according to call
            [~,min_dim] = min(size(obj.data));
            if  min_dim ~= 2
                obj.data = obj.data.';
            end
            obj.win = obj.window;
            obj.nchannels = size(obj.data,2);
            obj.nsamples = size(obj.data,1);
            obj.siglength = obj.nsamples/obj.fs;
            obj.sampleindex = (1:obj.nsamples).';
            obj.sampletimes = (obj.sampleindex - 1)/obj.fs; 
            obj.N = obj.framelength*obj.fs;
            obj.L = obj.frameshift*obj.fs;
            if isempty(obj.nfft)
               obj.nfft = obj.N*obj.FFT_factor; 
            end
            if obj.nsamples > obj.N
                obj.framed_t = FramingAndWindowing(obj.data,obj);
                obj.nframes = size(obj.framed_t,2);
            else
                obj.framed_t = obj.data.*obj.window(obj.N);
                obj.nframes = 1;
            end
            obj.framed_f_full = fft(obj.framed_t,obj.nfft);
            obj.framed_f = obj.framed_f_full(1:(obj.nfft/2+1),:);
            obj.framed_f = obj.framed_f/sum(obj.window(obj.N));
            
            obj.t_axis = obj.framelength/2 + (0:(obj.nframes-1))*obj.frameshift;
            obj.f_axis = ((0:(obj.nfft/2))*obj.fs/obj.nfft).';
            
            if ~isempty(obj.f0)
                param.fs = obj.fs;
                param.win = obj.win;
                param.FFT_factor = obj.FFT_factor;
                param.ApproxFramelength = obj.framelength;
                param.hop_factor = obj.frameshift/obj.framelength;
                param.f0_min = 90;
                param.f0_max = 350;
                param.FFT_factor = 2;
          %        if ~strcmp(obj.f0,'pefac')
          %              [obj.framed_f_PS, WindowImpact, obj.t_PS, idx_start,idx_end,fftlength,NDFT_max,t,f0_sig,HarmonicToDFT,DATA] = PS_swDFT(obj.data,obj.f0,obj.t_f0,param);
          %              obj.framed_f_PS_pn = PS_PhaseNormalization(obj.framed_f_PS,WindowImpact,f0_sig,obj.fs,NDFT_max,t,obj.t_PS,HarmonicToDFT);
          %        end
                param.f0_max = 350;
                param.FFT_factor = 1;
                if strcmp(obj.f0,'pefac')
                          [obj.framed_f_PA, obj.framed_f_PA_pn, obj.f_axes_PA,obj.f0,obj.t_f0] = PA_swDFT(obj.data, ...
                                                                        'framelength', obj.framelength, ...   % time domain frame length
                                                                        'frameshift', obj.frameshift, ...    % sliding window shift
                                                                        'f0_min', param.f0_min,.... % minmum fundamental frequency
                                                                        'f0_max', param.f0_max,... % maximum fundamental frequency
                                                                        'f0', [], ... % default fundmanetal frequency input
                                                                        't_f0', obj.t_f0, ... % default fundmanetal frequency input
                                                                        'fs', obj.fs,... % sampling frequency
                                                                        'window', obj.window ... % chosen window function
                                                                        );
                else
                    [obj.framed_f_PA, obj.framed_f_PA_pn, obj.f_axes_PA] = PA_swDFT(obj.data, ...
                                                                        'framelength', obj.framelength, ...   % time domain frame length
                                                                        'frameshift', obj.frameshift, ...    % sliding window shift
                                                                        'f0_min', param.f0_min,.... % minmum fundamental frequency
                                                                        'f0_max', param.f0_max,... % maximum fundamental frequency
                                                                        'f0', obj.f0, ... % default fundmanetal frequency input
                                                                        't_f0', obj.t_f0, ... % default fundmanetal frequency input
                                                                        'fs', obj.fs,... % sampling frequency
                                                                        'window', obj.window ... % chosen window function
                                                                        );
                end
                
            else
                    
            end

            
        end
        
        function obj= OLA(obj,varargin)
            if strcmp(varargin,'PA')
               
            elseif strcmp(varargin,'STFT')
                obj.framed_t = rifft(obj.framed_f,obj.nfft);
            end
            obj.data = STFT_OLA(obj.framed_t,obj);
        end
        
        function play(obj,varargin)
            if isempty(varargin)
                samples = 1:obj.nsamples;
            else
                samples = varargin{1};
                samples = samples(samples<obj.nsamples);
            end
            soundsc(obj.data(samples),obj.fs)
        end
        
        function plot_spectrogram(obj)
            tf = 20*log10(abs(obj.framed_f));
            plot_tf(obj,tf)
            %colormap(bone)
        end 
        
        function plot_spectrogram_PS(obj)
            tf = 20*log10(abs(obj.framed_f_PS));
            plot_tk(obj,tf)
            colormap(bone)
        end 
        
        function plot_spectrogram_paper(obj)
            tf = 20*log10(abs(obj.framed_f));%./max(max(abs(obj.framed_f)))); %
            plot_tf_paper(obj,tf)
            %colormap(1 - gray)
        end 
        
        function plot_time(obj)
            plot(obj.sampletimes,obj.data)
            xlabel('Time (s)')
            grid on
            axis tight
            box on
        end
        
        function plot_freq(obj)
            plot(obj.f_axis,abs(obj.framed_f))
            xlabel('Frequency (Hz)')
            grid on
            axis tight
            box on
        end
        
        function plot_tf(obj,tf)
            imagesc(obj.t_axis,obj.f_axis/1e3,tf)
            set(gca,'YDir','normal')
            ylabel('Frequency (kHz)')
            xlabel('Time (s)')
            box on
            colorbar
        end   
        
        function plot_tk(obj,tf)
            imagesc(obj.t_axis,(0:(size(tf,2))),tf)
            set(gca,'YDir','normal')
            ylabel('Frequency Bin')
            xlabel('Time (s)')
            box on
            colorbar
        end   
        
        function plot_tf_paper(obj,tf)
            imagesc(obj.t_axis,obj.f_axis/1e3,tf)
            set(gca,'YDir','normal')
            set(gca,'XTickLabel',{})
            set(gca,'YTickLabel',{})
            set(gca,'YTick',[])
            set(gca,'XTick',[])
            

            ax = gca;
            outerpos = ax.OuterPosition;
            ti = ax.TightInset; 
            left = outerpos(1) + ti(1);
            bottom = outerpos(2) + ti(2);
            ax_width = outerpos(3) - ti(1) - ti(3);
            ax_height = outerpos(4) - ti(2) - ti(4);
            ax.Position = [left bottom ax_width ax_height];
            %ylabel('Frequency (kHz)')
            %xlabel('Time (s)')
            box on
            %colorbar
        end   
        
        function stream_audio(obj)
            
        end
        
        function save(obj,path,filename)
            x = obj.data/max(abs(obj.data) +eps);
            audiowrite([path filename], x, obj.fs);
        end
        
        
    end
    
end

