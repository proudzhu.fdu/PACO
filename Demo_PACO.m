function Demo_PACO()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Illustrates two ways to perform speech enhancement via the pitch-adaptive 
% discrete short-time Fourier transform (PADSTFT) proposed in [1] and [4].
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% author: Johannes Stahl (johannes.stahl@tugraz.at)
% last update: 14.01.2019
% matlab version: 2017b
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% References:
% [1] J. Stahl and P. Mowlaee "Exploiting Temporal Correlation in 
%     Pitch-Adaptive Speech Enhancement", submitted to 
%     Speech Communication, 2018.
% [2] J. S. Garofolo, L. F. Lamel, W. M. Fisher, J. G. Fiscus, D. S.
%     Pallett, and N. L. Dahlgren, “DARPA TIMIT acoustic phonetic
%     continuous speech corpus CDROM,” 1993. [Online]. Available:
%     http://www.ldc.upenn.edu/Catalog/LDC93S1.html
% [4] J. Stahl and P. Mowlaee "A Simple and Effective Framework for A priori SNR estimation", 
%     appear in Proc. ICASSP 2018.
% [5] VOICEBOX: MATLAB toolbox for speech processing.
%     Home page: http://www.ee.ic.ac.uk/hp/staff/dmb/voicebox/voicebox.html
% [6] Y. Ephraim and D. Malah, “Speech enhancement using a minimum mean-square
%     error log-spectral amplitude estimator,” IEEE Trans. Acoust., Speech, Signal Pro-
%     cess., vol. 33, no. 2, pp. 443–445, Apr 1985.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% bugs, comments, etc:
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     Demo_PitchAdaptiveSpeechEnhancement.m illustrates speech enhancement
%     via the PADSTFT.
%     Copyright (C) 2018 Johannes Stahl
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

addpath('./Functions/')
addpath(genpath('./ExternalFunctions/')) % these are functions from [4], needed for f0-estimation

% load noisy speech file:
[y, fs]= audioread('./Audio/dr6_fmgd0_sx124_white_16k_10_Noisy.wav');% from [2]

% run PADDi [1]/[3] + Wiener Filter:
xhat_PADDi = PADDi(y,...
                'fs',fs,...
                'framelength', 0.032, ...
                'Estimator', 'WienerFilter'); 

% run PACO [1]:
xhat_PACO = PACO(y,...
                'fs',fs,...
                'framelength', 0.032); 

% run LSA [6]:
xhat_LSA = STFT_SpeechEnhancement(y,...
                                  'fs',fs, ...
                                  'framelength', 0.032, ...
                                  'Estimator', 'LSA');  


% plot and play the audiosignals
y = audiosignal(y);
xhat_LSA = audiosignal(xhat_LSA);
xhat_PACO = audiosignal(xhat_PACO);
xhat_PADDi = audiosignal(xhat_PADDi);

figure
subplot(221)
y.plot_spectrogram; caxis([-80 -30]); colorbar; title('Noisy')
subplot(222)
xhat_LSA.plot_spectrogram; caxis([-80 -30]); colorbar; title('LSA')
subplot(223)
xhat_PADDi.plot_spectrogram; caxis([-80 -30]); colorbar; title('PADDi')
subplot(224)
xhat_PACO.plot_spectrogram; caxis([-80 -30]); colorbar; title('PACO')

y.play; pause(y.siglength)
xhat_LSA.play; pause(xhat_LSA.siglength)
xhat_PADDi.play; pause(xhat_PADDi.siglength)
xhat_PACO.play


end


